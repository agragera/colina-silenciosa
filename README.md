# ColinaSilenciosa

Controles:

-Movimiento -> WASD
-inventario -> i
-Dropear item -> click sobre el item en el inventario

Risketos:

-Sistema de inventario donde pulsas sobre el item y lo dropeas
-Mediante la iluminación te resalto el item a obtener y deberas buscar la salida
-Inteligencia artificial con sistema de guardia por zona y seguimiento del jugador con golpe para matarlo
-Animaciones para el enemigo al seguirte y golpearte
-Opcion para implementar la muerte del jugador (No he hecho que muera como tal el jugador pero asi se terminaria el juego)
-Sistema de puzzle sencillo dónde tiras el item encima y te desbloquea el camino (tirar la llave adecuada al "pedestal")
-Iluminación "realista" con niebla volumétrica (Me ha costado encontrar el punto entre estar ciego o tener vista de lince)
