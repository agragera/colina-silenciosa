// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ColinaSilenciosaGameMode.generated.h"

UCLASS(minimalapi)
class AColinaSilenciosaGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AColinaSilenciosaGameMode();
};



