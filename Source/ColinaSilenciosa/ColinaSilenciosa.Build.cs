// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ColinaSilenciosa : ModuleRules
{
	public ColinaSilenciosa(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
